# CSCI460MinExpressMongo

Mr. Castleton's CSCI 460 Minimum Express, Mongo demonstration application

## Description
This is a minimal demonstration of a NodeJS Express webserver with MongoDB as database back-end to show how 'Vanilla' NodeJS, Express, MongoDb, DotEnv can be used to create a web based application that demonstraton CRUD.  Create, Retrieve, Update, Delete mongo.  

## Installation
Find guides online to install MongoDB, Nodejs and NPM. 

I tested this on Ubuntu Linux.  The files in this repository are what is needed in the root(.), src and public folders that are part of an Express server. I have included the package.json but you should probably delete that and use npm init in the folder where you clone this repository and then npm install mongodb, dotenv and express.  Exact command lines can be found online.  This has been demonstrated in class.

When you have configured your machine to have MongDB, Nodejs and have installed a Express, dotenv, and mongodb for node in the folder, you should be able to type 
"node src/index.js" 
and get a message the webserver is running on port 3000.  

## Usage
While you are running the Express Server you should be able to have a browser go to 
"localhost:3000" and you should see the Add, Update, Delete a Person forms from public/index.html.

You can install on Linux systems a program called pm2 which will allow for startup of src/index.js when a machine boots.  But this is meant as a minimum learning example not a production application.  

## Support
If you are a Computer Science student at Colorado Mesa University please send me an email, or catch me in the hallways.  If you are not a Colorado Mesa University student first, consider becoming one if you are looking at this project, second, send an email to the one that can be found at https://www.coloradomesa.edu.  Quickest method is using google to search "site:coloradomesa.edu castleton" should yield a number of ways of contacting me.  

## Roadmap
In the Spring 2023 there will probably be an update to make this application a minimal Progressive Web App and fix responsive design issues.

## Contributing
If you want to contribute please contact me.  I am using this as material for a Database class in the spring semester 2023.  But I am open to any collaboration.
