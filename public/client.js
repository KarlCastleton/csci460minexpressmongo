
class Person{
    constructor(elements) {  // construct a person from elements of a form
        this.first="";
        this.last="";
        this.address="";
        this.salary="";
        if (elements==null) return;
        if (elements.first) this.first=elements.first.value;
        if (elements.last) this.last=elements.last.value;
        if (elements.address) this.address=elements.address.value;
        if (elements.salary) this.salary=elements.salary.value;
    }
    setFrom(json) {
        this.first=json.first;
        this.last=json.last;
        this.address=json.address;
        this.salary=json.salary;
    }
    static add(handle) {
        let data=new Person(handle.elements);
        fetch(`add?first=${data.first}&last=${data.last}&address=${data.address}&salary=${data.salary}`);
    }
    static update(handle) {
        let data=new Person(handle.elements);
        fetch(`update?first=${data.first}&salary=${data.salary}`);
    }
    static delete(handle) {
        let data=new Person(handle.elements);
        fetch(`delete?first=${data.first}`);
    }
    generateRow() {
        return `<tr><td>${this.first}</td><td>${this.last}</td><td>${this.address}</td><td>${this.salary}</td></tr>`;
    }
}

class People {
    constructor(json) {
        this.people=new Array();
        json.forEach((e)=>{
          let p=new Person();
          p.setFrom(e);
          this.people.push(p);
        });
    }
    generateTable() {
        var t="<table>";
        t+=`<tr><th>First</th><th>Last</th><th>Address</th><th>Salary</th></tr>`;
        this.people.forEach((p)=>{
            t+=p.generateRow();
        });
        t+="</table>";
        return t;        
    }
}

function getAllFromServer() { // Get All People and put in a table
    fetch("getAll")
    .then((response)=>response.json())
    .then((data)=>{
        if (data!=null) {
            let all=new People(data);
            document.getElementById('content').innerHTML=all.generateTable();
        }
      }).catch((error) => {
      console.error("Error:", error);
    });
  }
  