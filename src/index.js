import {addPerson} from './person.js';
import {deletePerson} from './person.js';
import {updatePerson} from './person.js';
import {getAll} from './person.js';
import express from 'express';

const app = express()
const port = 3000

app.use(express.static('public'));

app.get('/add',(req,res)=>{
  console.log("Add ",req.query);
  addPerson(req.query);
})

app.get('/update',(req,res)=>{
  console.log("Update ",req.query);
  updatePerson(req.query);
})

app.get('/delete',(req,res)=>{
  console.log("Delete ",req.query);
  deletePerson(req.query);
})

app.get('/getall', (req, res) => {
  console.log("GetAll",req.query);
  getAll().then((data)=>{
    console.log(JSON.stringify(data));
    res.send(JSON.stringify(data));
  });
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
