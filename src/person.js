import { MongoClient } from 'mongodb';
import {config} from 'dotenv';

config();
const mongoClient=new MongoClient(process.env.DB_URI);
const db=mongoClient.db('myapp');
const people=db.collection('people');

export function updatePerson(obj) {
  people.updateOne({first:obj.first},{$set:{salary:obj.salary}});
}

export function addPerson(obj) {
    people.insertOne(obj);
  }

export function deletePerson(obj) {
    people.deleteOne({first:obj.first});
  }

export  function getAll() {
    return people.find().project({_id:0}).toArray();
}
